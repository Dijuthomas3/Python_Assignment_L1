#Write a program to create two list A and B such that List A contains Employee Id, List B contains Employee name
#     ( minimum 10 entries in each list ) and perform following operations
#     a) Print all names on to screen
#     b) Read the index from the  user and print the corresponding name from both list.
#     c) Print the names from 4th position to 9th position
#     d) Print all names from 3rd position till end of the list
#     e) Repeat list elements by specified number of times ( N- times, where N is entered by user)
#     f)  Concatenate two lists and print the output.
#     g) Print element of list A and B side by side.( i.e.  List-A First element ,  List-B First element )


list1=input("Enter name of 10 employees in list: ")
list2=input("Enter 10 employee ID's in list: " )
n=input("Enter the index number to print elements side by side: ") 
N=input("Enter number of times to repeat elements of list: ") 
m=len(list1);j=len(list2) 

for i in range(len(list1)):
    print "Names are: ", list1[i]

print "Required info from both list with index no from user: ", list1[n],list2[n] 
print "Names from 4th to 9th position: ",list1[4:9]
print "Names from 3rd to last position: ",list1[3:len(list1)] 
print "repeated list items are: ", list1*N,list2*N 
list3=list1+list2 
print "Concatenated list: ", list3
if (m==j):
    for i in range(m):
        print list2[i],list1[i] 
else:
    print "The no. of elements in both list is not same"
