# Using loop structures print even numbers between 1 to 100.  
#     a) By using For loop , use continue/ break/ pass statement to skip  odd numbers.
#           i)  break the loop if the value is 50
#           ii) Use continue for the values 10,20,30,40,50
#     b) By using while loop, use continue/ break/ pass statement to skip odd numbers.
#            i)  break the loop if the value is 90
#           ii) Use continue for the values 60,70,80,90

for i in range(1,100):
	if (i == 50):
		print "Breaking at 50"
		break 
	if (i%2==0):
		print "Even numbers between 1 and 100: \n", i
for i in range(1,100):
	if (i==10 or i==20 or i==30 or i==40 or i==50):
		print "Skipping  using continue"
		continue 
	if(i%2==0):
		print "Even no's b/w 1 and 100: \n", i

n=1
while n<101:
	if (n==50):
		print "Breaking after 50" 
		break 
	if(n%2==0):
		print "Even numbers b/w 1 and 100:\n ", n
	n=n+1

p=0
while p<101:
	p=p+1 
        if(p%2==0):
                print "Even no's b/w 1 and 100: \n", p
		if (p == 10 or p == 20 or p == 30 or p == 40 or p == 50):
                	print "Skipping  using continue"
                	continue 
