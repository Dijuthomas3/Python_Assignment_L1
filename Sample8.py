#  Create a tuple with at least 10 elements in it 
# print all elements 
# perform slicing 
# perform repetition with * operator 
# Perform concatenation wiht other tuple.

tup1=(2,3,5,7,'a', 9,11) 
tup2=(1,4,6,8,10,'b',12)

print tup1
print tup2

tup3=tup1[2:6]
tup4=tup1*3
tup5=tup1 + tup2

print tup3
print tup4
print tup5
