# Using loop structures print numbers from 1 to 100.  and using the same loop print numbers from 100 to 1.( reverse printing)
# a) By using For loop 
# b) By using while loop
# c) Let    mystring ="Hello world". print each character of  mystring in to separate line using appropriate  loop structure.


for i in range(1,101):
    print  i,",",
print "\n----------------------------------------------------------"

for j in range(100,0,-1):
    print  j,",",
print "\n----------------------------------------------------------"

n=0
while (n<101):
    print n,",",
    n=n+1
print "\n----------------------------------------------------------"

k=100
while (k>0):
    print k,",",
    k=k-1
print "\n----------------------------------------------------------"

mystring= "Hello world"
num=len(mystring)
for i in range(num):
    print mystring[i]
    
